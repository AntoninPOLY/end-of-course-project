export const state = () => ({  cards: [] })

export const actions = {
  async getCards({ state, commit }) {
    let res = await this.$axios.get('/api/cards')
    let amounts = await this.$axios.get('/api/assets/amount')
    console.log(amounts.data)
    const cards = []

    for(const card of res.data) {
      const assets = []
      if(card.assets.length > 0) {
        card.assets.forEach((asset) => {
          const i = amounts.data.map((amount) => amount.asset_id).indexOf(asset.asset_id)
          console.log(amounts)
          assets.push({
            url: asset.image ? asset.image.url : '',
            id: asset.asset_id,
            price: asset.price,
            rarity: asset.rarity,
            amount: amounts.data[i] ? amounts.data[i].amount : 0
          })
        })
      }
      cards.push({
        id: card.id,
        name: card.name,
        hp: card.hp,
        ad: card.ad,
        assets: assets ? assets : [],
      })
    }
    commit('cards', cards)
  },
  async createCard({state, commit}, formData) {
    const res = await this.$axios.post('/api/cards/create', formData)
    const card = {
      id: res.data.id,
      name: res.data.name,
      hp: res.data.hp,
      ad: res.data.ad,
      assets: [],
    }
    commit('add', card)
    return res
  },
}

export const mutations = {
  cards(state, cards) {
    state.cards = cards
  },
  add(state, card) {
    state.cards.push(card)
  }
}

export const getters = {
  cards: (state) => {
    return state.cards
  }
}
