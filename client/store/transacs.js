
export const state = () => ({  collection: [], isConnected: false, address: ''})

export const actions = {
  async connect({state, commit}, address) {
    const res = await this.$axios.get('https://testnet.algoexplorerapi.io/v2/accounts/'+address)
    const collection = res.data.assets.length > 0 ? res.data.assets.map(asset => asset['asset-id']) : []
    const connected = res.data.address ? true : false
    commit('login', {
      connected,
      collection,
      address: res.data.address
    })
    return res
  },
  async getAsset({state, commit}, id) {
    return await this.$axios.get('/api/assets/get/'+id)
  }
}

export const mutations = {
  login(state, params) {
    state.address = params.address
    state.isConnected = params.connected
    state.collection = params.collection
  }
}

export const getters = {
  isConnected: (state) => {
    return state.isConnected
  },
  getCollection: (state) => {
    return state.collection
  },
  getAddress: (state) => {
    return state.address
  }
}
