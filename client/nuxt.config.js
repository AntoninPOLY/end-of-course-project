export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'algomond',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: { color: "#3B8070"},
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/particles',
    '@/plugins/myalgo.client',
    '@/plugins/algosdk.client',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/proxy',
    [
      'nuxt-fontawesome', {
        imports: [
          {
            set: '@fortawesome/free-solid-svg-icons',
            icons: ['fas']
          },
          {
            set:'@fortawesome/free-brands-svg-icons',
            icons: ['fab']
          }
        ]
      },
    ]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    proxy: true,
    headers: {
      common: {
        'Accept': 'application/json, text/plain, */*'
      },
      delete: {},
      get: {},
      head: {},
      post: {},
      put: {},
      patch: {}
    }
  },
  proxy: {
    '/api': {
      target: process.env.NODE_ENV === 'production'
      ? 'https://algomond-api.herokuapp.com'
      : 'http://localhost:5000',
      pathRewrite: {
        '^/api/': ''
      }
    }
  },
  auth: {
    redirect: {
      login: '/algomond-login',
      logout: '/algomond-login',
      home: '/algomond-admin'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/api/login",
            method: "post",
            propertyName: "token",
          },
          logout: false,
          user: {
            url: "/api/verify",
            method: "get",
            propertyName: "data",
          },
        },
      },
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      ({isClient}) => isClient && '@randlabs/myalgo-connect',
      ({isClient}) => isClient && 'algosdk'
    ]
    //
  }
}
