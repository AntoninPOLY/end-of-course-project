import algosdk from 'algosdk'

export default ({app}, inject) => {
   inject('algosdk', algosdk)
}
