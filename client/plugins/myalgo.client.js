import MyAlgo from '@randlabs/myalgo-connect'

export default ({app}, inject) => {
  const myAlgo = () => {
    return new MyAlgo()
  }
  inject('myalgo', myAlgo)
}
