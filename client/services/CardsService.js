export default class CardsService {
    static getCards() {
            return new Promise(async (resolve, reject) => {
            try {
                const res = await this.$axios.get('https://algoexplorerapi.io/idx2/v2/assets?name=algomond')
                resolve(res.data);
            } catch (err) {
                reject(err);
            }
        })
    }
}
