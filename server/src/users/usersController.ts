// @ts-nocheck

import * as usersDAL from './usersDAL'
import {UserInput, UserOutput} from './index'

export default class UsersController {
    static async getUsers(): Promise<UserOutput[]> {
        console.log("\t\tUsersController@getUsers")
        const Users = await usersDAL.get()
        return Users;
    }

/**
   * Returns a User Model corresponding to an id.
   *
   * @remarks
   * This method is part of the {@link core-library#Users | User subsystem}.
   *
   * @param id - The to search a user
   * @returns A User Model with id === id
   *
   * @beta
   */
    static async getUserById(id) {
        console.log("\t\tUsersController@getUserById")
        const User = await usersDAL.findOne({id: id})
        return User
    }

    static async createUser(body: UserInput) {
        console.log("\t\tUsersController@createUser")
        const User = await usersDAL.create(body)
    }
}
