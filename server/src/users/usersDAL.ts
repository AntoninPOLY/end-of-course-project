// @ts-nocheck

import User from './index'

export const get = async () => {
    console.log("\t\tUsersDAL@get");
    const users = await User.findAll()
    return users
}

export const findOne = async (whereClause)=> {
    console.log("\t\tUsersDAL@findOne");
    return await User.findOne({
        where: whereClause
    })
}

export const update = async (updateClause, whereClause)=> {
    console.log("\t\tUsersDAL@update");
    return await User.update(updateClause, {
        where: whereClause
    })
}

export const create = async (body)=> {
    console.log("\t\tUsersDAL@create");
    return await User.create({
        username: body.username,
        password: body.password
    })
}
