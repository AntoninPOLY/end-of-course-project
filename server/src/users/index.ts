import { DataTypes, Model } from 'sequelize'
import sequelizeConnection from '../db/config'
import bcrypt from 'bcrypt'

export default class User extends Model {
    public id!: number
    public username!: string
    public password!: string
    public token!: string
    public role!: number

    public readonly createdAt!: Date
    public readonly updatedAt!: Date
    public readonly deletedAt!: Date

}

User.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull:false,
        get() {
            return () => this.getDataValue('password')
        }
    },
    role: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        defaultValue: 0,
        get() {
            return () => this.getDataValue('role')
        },
    },
    token: {
        type: DataTypes.STRING,
        allowNull: true,
        unique: true,
        get() {
            return () => this.getDataValue('token')
        },
    },
}, {
    timestamps: true,
    sequelize: sequelizeConnection,
    modelName: 'user',
})

async function hashPassword(password) {
    console.log('hash password : ' + password)
    const hash = await bcrypt.hash(password, 12)
    console.log(hash)
    return hash

}

async function setPassword(user) {
    if(user.changed('password')) {
        user.password = await hashPassword(user.password())
        console.log('password saved')
    }
}

User.beforeCreate(setPassword)
User.beforeUpdate(setPassword)
