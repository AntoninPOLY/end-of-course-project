// @ts-nocheck
import algosdk from 'algosdk'

const token = {
  'X-API-key': process.env.ALGO_TOKEN
}
const algoServer = process.env.ALGO_SERVER

console.log(token)
const client = new algosdk.Algodv2(token, algoServer, '')

export default client
