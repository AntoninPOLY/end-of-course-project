// @ts-nocheck

import { Router } from '@awaitjs/express'
import {authenticateToken} from '../middlewares/auth'
import {checkCard} from '../middlewares/cards'
import cardsController from './cardsController'

const router = Router()

router.getAsync("/", async function(_, res) {
  let result = await cardsController.getCards();
  res.send(result)
});


router.getAsync("/delete/:id", [authenticateToken], async function(req, res) {
  let result = await cardsController.deleteCard(req.params.id);
  res.send(result)
});

router.postAsync("/create", [authenticateToken, checkCard], async function(req, res) {
  let result = await cardsController.createCard(req.body)
  res.send(result)
})

export default router
