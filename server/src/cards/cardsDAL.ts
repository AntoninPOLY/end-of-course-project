
// @ts-nocheck

import Card from './index'

export const get = async () => {
    console.log("\t\tCardDAL@get");
    return await Card.findAll({
    })
}

export const findOne = async (whereClause) => {
    console.log("\t\tCardDAL@findOne");
    return await Card.findOne({
        where: whereClause
    });
}

export const update = async (updateClause, whereClause) => {
    console.log("\t\tCardDAL@update");
    return await Card.update(updateClause, {
        where: whereClause
    });
}

export const create = async (body) => {
    console.log("\t\tCardDAL@create");
    return await Card.create({
        name: body.name,
        hp: body.hp,
        ad: body.ad
    })
}
