
// @ts-nocheck

import {  DataTypes, Model } from 'sequelize'
import sequelizeConnection from '../db/config'

import Asset from '../assets'
import Image from '../images'

export default class Card extends Model {}

Card.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  hp: {
    type: DataTypes.INTEGER,
    allowNull: false,
    default: 0
  },
  ad: {
    type: DataTypes.INTEGER,
    allowNull: false,
    default: 0
  },
}, {
  defaultScope: {
    include: {
      model: Asset,
      as: 'assets',
      include: {
        model: Image,
        as: 'image'
      }
    }

  },
  paranoid: true,
  timestamps: true,
  modelName: 'card',
  sequelize: sequelizeConnection,
})

Card.Assets = Card.hasMany(Asset)
