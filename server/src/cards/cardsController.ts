// @ts-nocheck
import * as cardsDal from './cardsDAL'
import * as imagesDal from '../images/imagesDAL'

export default class CardsController {
    static async getCards() {
        console.log("\tCardController@getCards")
        const cards = await cardsDal.get()
        if(cards) {
            return cards
        }
        return false
    }

    static async getCardByAssetId(id: number) {
        console.log("\tCardController@getCardByAssetId")
        const card = await cardsDal.findOne({asset_id: id})
        if(card) {
            return card
        }
        return false

    }

    static async createCard(body) {
        console.log("\tCardController@createCard");
        const res = await cardsDal.create(body)
        if(res) {
            return res
        }
        return false
    }

    static async deleteCard(id) {
        const card = await cardsDal.findOne({id: id})
        if(card) {
            const res = await card.destroy()
        }
        return res ? true : false
    }
}
