// @ts-nocheck

import algodClient from '../algo/config'
import {LogicTemplates} from "algosdk"
import * as transacDal from './transacDAL'
import * as assetsDal from '../assets/assetsDAL'
import algosdk from 'algosdk'
import axios from 'axios'

export default class TransacController {
  static async getTransacs() {
    console.log("\tCardController@getCards")
    const res = await transacDal.get()
    if(res) {
      return res
    }
    return false
  }

  static async createAccount() {
    console.log("\tTransacController@createAccount");

    const {addr, sk} = algosdk.generateAccount()
    const mnemonic = algosdk.secretKeyToMnemonic(sk)

    return {
      addr, mnemonic
    }
  }

  static async amountLeft() {
    console.log("\tTransacController@amountLeft");
    const account1_mnemonic = process.env.ALGO_ESCROW
    const {addr, sk} = algosdk.mnemonicToSecretKey(account1_mnemonic);
    const res = await axios.get('https://testnet.algoexplorerapi.io/v2/accounts/'+addr)
    console.log(res.data)
    const assets = []
    res.data.assets.map((asset) => assets.push({
      asset_id: asset['asset-id'],
      amount: asset.amount
    }))
    return assets
  }

  static async assetLeft(asset_id) {
    console.log("\tTransacController@assetLeft");
    const account1_mnemonic = process.env.ALGO_ESCROW
    const {addr, sk} = algosdk.mnemonicToSecretKey(account1_mnemonic);
    const res = await axios.get('https://testnet.algoexplorerapi.io/v2/accounts/'+addr)
    const assets = res.data.assets
    let amount = 0
    assets.forEach((asset) => {
      if(asset['asset-id'] == asset_id) amount = asset.amount
    })
    return {amount}
  }

  static async createTransaction(body, id) {
    console.log("\tTransacController@createTransaction");
    const params = await algodClient.getTransactionParams().do();
    const asset = await assetsDal.findOne({asset_id: id})
    const template = await this.algoCreateTransaction(body, asset)
    let contract = await transacDal.findOne({address: template.address})
    if(!contract) {
      await transacDal.create(template)
    } else if (contract){
      await transacDal.update({program: template.program}, {address: template.address})
    }
    contract = await transacDal.findOne({address: template.address})
    console.log(contract)
    const res = {
      address: contract.address,
      asset_id: contract.asset_id,
      params: params
    }

    return res
  }

  static async sendTransaction(body, contract) {
    const txns = []
    body.txns.forEach((txn) => {
      const res = new Uint8Array(atob(txn).split("").map(
        (char)=>char.charCodeAt(0)
      )
    )
    txns.push(res)
  })
  await algodClient.sendRawTransaction(txns).do();

  const transac = await transacDal.findOne({address: contract})
  const asset = await assetsDal.findOne({asset_id: transac.asset_id})
  const program = transac.program
  const account1_mnemonic = process.env.ALGO_ESCROW
  const {sk} = algosdk.mnemonicToSecretKey(account1_mnemonic);
  const txId = await this.algoSendTransaction(program, sk, asset)
  return txId
}

static async algoSendTransaction(program, sk, asset) {
  const params = await algodClient.getTransactionParams().do();
  let endRound = params.firstRound + parseInt(1000);

  let assetAmount = parseInt(1);
  const price = algosdk.algosToMicroalgos(asset.price)
  let microAlgoAmount = parseInt(price);
  let secretKey = sk;

  let txnBytes = LogicTemplates.getSwapAssetsTransaction(program, assetAmount,
    microAlgoAmount, secretKey, params.fee, params.firstRound, endRound, params.genesisHash);
    const {txId} = (await algodClient.sendRawTransaction(txnBytes).do());
    console.log( "txId: " + txId );
    return txId
  }


  static async  algoCreateTransaction(body, asset) {
    console.log("\t\t\tTransacDAL@algoCreateTransaction")

    const owner = await axios.get('https://testnet.algoexplorerapi.io/v2/accounts/'+body.address)
    let ownerAddress = owner.data.address

    const price = algosdk.algosToMicroalgos(asset.price)
    let ratn = parseInt(1);
    let ratd = parseInt(price);
    console.log(price)
    let assetID = parseInt(asset.asset_id);
    let expiryRound = 10;

    let maxFee = 2000;
    let minTrade = price - maxFee;
    let limit = new LogicTemplates.LimitOrder(ownerAddress, assetID, ratn,ratd, expiryRound, minTrade, maxFee);

    let program = limit.getProgram();
    const address = limit.getAddress()
    return {
      address,
      program: program,
      owner: limit.owner,
      assetId: limit.assetid
    }
  }

  static async waitForConfirmation(algodClient, txId, timeout = 60000) {
    let { 'last-round': lastRound } = await algodClient.status().do();
    while (timeout > 0) {
      const startTime = Date.now();
      // Get transaction details
      const txInfo = await algodClient.pendingTransactionInformation(txId).do();
      if (txInfo) {
        if (txInfo['confirmed-round']) {
          return txInfo;
        } else if (txInfo['pool-error'] && txInfo['pool-error'].length > 0) {
          throw new Error(txInfo['pool-error']);
        }
      }
      // Wait for the next round
      await algodClient.statusAfterBlock(++lastRound).do();
      timeout -= Date.now() - startTime;
    }
    throw new Error('Timeout exceeded');
  }
}
