import { DataTypes, Model } from 'sequelize'
import sequelizeConnection from '../db/config'


export default class Transaction extends Model {}

Transaction.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true
  },
  address: {
    type: DataTypes.STRING,
    unique: true
  },
  program: {
    type: DataTypes.BLOB('long')
  },
  owner: {
    type: DataTypes.STRING,
  },
  asset_id: {
    type: DataTypes.INTEGER,
  }
}, {
  timestamps: true,
  sequelize: sequelizeConnection,
})
