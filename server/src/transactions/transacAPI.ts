// @ts-nocheck

import { Router } from '@awaitjs/express'
import transacController  from './transacController'

const router = Router()

router.getAsync('/', async function(_, res ) {
  const result = await transacController.createAccount()
  res.send(result)
})


router.getAsync('/all', async function(_, res ) {
  const result = await transacController.getTransacs()
  res.send(result)
})

router.post('/buy/:id', async function(req, res) {
  const result = await transacController.createTransaction(req.body, req.params.id)
  res.send(result)
})

router.postAsync('/save/:contract', async function(req, res ) {
  const result = await transacController.sendTransaction(req.body, req.params.contract)
  res.status(200).send(result)
})

export default router
