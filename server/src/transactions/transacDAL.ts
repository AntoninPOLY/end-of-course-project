
// @ts-nocheck

import Transaction from './index'

export const get = async (whereClause) => {
  console.log("\t\ttransacDAL@get");
  return await Transaction.findAll(whereClause)
}

export const findOne = async (whereClause) => {
  console.log("\t\ttransacDAL@findOne");
  return await Transaction.findOne({
    where: whereClause
  })
}

export const update = async (updateClause, whereClause) => {
  console.log("\t\ttransacDAL@update");
  return await Transaction.update(updateClause, {
    where: whereClause
  })
}

export const create = async (body) => {
  console.log("\t\ttransacDAL@create");
  return await Transaction.create({
    address: body.address,
    program: body.program,
    owner: body.owner,
    asset_id: body.assetId
  })
}
