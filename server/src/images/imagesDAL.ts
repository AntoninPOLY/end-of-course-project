// @ts-nocheck

import Image from './index'

export const get = async (whereClause) => {
    console.log("\t\ImageDAL@get");
    return await Image.findAll(whereClause)
}

export const findOne = async (whereClause) => {
    console.log("\t\ImageDAL@findOne");
    return await Image.findOne({
        where: whereClause
    })
}

export const update = async (updateClause, whereClause) => {
    console.log("\t\ImageDAL@update");
    return await Image.update(updateClause, {
        where: whereClause
    })
}

export const create = async (body) => {
    console.log("\t\ImageDAL@create");
    return await Image.create({
        image_id: body.image_id,
        url: body.url,
    })
}
