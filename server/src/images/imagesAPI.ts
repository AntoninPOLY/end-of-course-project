// @ts-nocheck
import { Router } from '@awaitjs/express'
import multer from 'multer'
import cloudinary from 'cloudinary'
import streamifier from 'streamifier'
import {authenticateToken} from '../middlewares/auth'
import imagesController from './imagesController'

const router = Router()
const storage = multer.memoryStorage()
const upload = multer({
    storage: storage,
})

router.postAsync('/upload', [authenticateToken, upload.single('image')], async function(req, res) {
    let result = await uploadFromBuffer(req)
    const public_id = result.public_id + '.' + result.format
    const image = await imagesController.uploadImage({
        url: result.url,
        image_id: public_id
    })
    res.send(image)
})

const uploadFromBuffer = (req) => {
    return new Promise((resolve, reject) => {
        let cld_upload_stream = cloudinary.v2.uploader.upload_stream(
            {
                folder: "cards"
            },
            (error: any, result: any) => {
                if (result) {
                    resolve(result)
                } else {
                    reject(error)
                }
            }
        )
        streamifier.createReadStream(req.file.buffer).pipe(cld_upload_stream);
    })
}

export default router
