
// @ts-nocheck
import * as imagesDal from './imagesDAL'

export default class ImagesController {
    static async getImages() {
        console.log("\tCardController@getCards");
        const result = await imagesDal.get();
        if(result) {
            return result
        }
        return false
    }

    static async getRecentImages() {
        console.log("\tCardController@getRecentCards");
        const result = await imagesDal.get({
            where: {
                createdAt: {
                    [Op.gt]: new Date(today.getTime() - (1000*60*60))
                }
            },
            order: ['createdAt', ['DESC']],
            raw: true
        })
        if(result) {
            return result
        }
        return false
    }

    static async uploadImage(body) {
        console.log("\tImagesController@uploadImage");
        const result = await imagesDal.findOne({image_id: body.image_id})
        console.log(result)
        if(result) {
            return {
                id: result.id,
                url: result.url,
                name: result.name,
            }
        } else if(!result) {
          const res = await imagesDal.create(body)
          return res
        } else {}
    }

}
