import { DataTypes, Model } from 'sequelize'
import sequelizeConnection from '../db/config'


export default class Image extends Model {}

Image.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true
  },
  image_id: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  url: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  }
}, {
  timestamps: true,
  modelName: 'image',
  sequelize: sequelizeConnection,
})
