// @ts-nocheck
import User from '../users'
import Cards from '../cards'
import Asset from '../assets'
import Image from '../images'
import Transaction from '../transactions'

import sequelizeConnection from './config'

const dbInit = async () => {
    await sequelizeConnection.sync({ force: true})


    await User.create({
        username: 'platoonaltocerulean',
        password : 'mmubwyNJ4Jyaqg',
        role: 1
    })

}

export default dbInit
