// @ts-nocheck

import { Dialect, Sequelize } from 'sequelize'

const dbName = process.env.MYSQL_DATABASE as string
const dbUser = process.env.MYSQL_USER as string
const dbHost = process.env.MYSQL_HOST
const dbPassword = process.env.MYSQL_PASSWORD

const sequelizeConnection = new Sequelize(dbName, dbUser, dbPassword, {
  host: dbHost,
  dialect: 'mysql',
  logging: false
})

export default sequelizeConnection
