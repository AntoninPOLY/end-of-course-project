

// @ts-nocheck

import Asset from './index'

export const get = async () => {
  console.log("\t\tAssetDAL@get")
  return await Asset.findAll({
  })
}

export const findOne = async (whereClause) => {
  console.log("\t\tAssetDAL@findOne")
  return await Asset.findOne({
    where: whereClause
  })
}

export const update = async (updateClause, whereClause) => {
  console.log("\t\tAssetDAL@update")
  return await Asset.update(updateClause, {
    where: whereClause
  })
}

export const create = async (body) => {
  console.log("\t\tAssetDAL@create")
  return await Asset.create({
    asset_id: body.asset_id,
    rarity: body.rarity,
    price: body.price
  })
}
