
// @ts-nocheck

import {  DataTypes, Model } from 'sequelize'
import sequelizeConnection from '../db/config'

import Image from '../images'

export default class Asset extends Model {}

Asset.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true
  },
  asset_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    unique: true
  },
  rarity: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  price: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0
  }
},{
  defaultScope: {
    include: {
      model: Image,
      as: 'image'
    }
  },
  timestamps: true,
  modelName: 'asset',
  paranoid: true,
  sequelize: sequelizeConnection,
})

Asset.Image = Asset.hasOne(Image)
