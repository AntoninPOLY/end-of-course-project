
// @ts-nocheck
import * as cardsDal from '../cards/cardsDAL'
import * as assetsDal from './assetsDAL'
import * as imagesDal from '../images/imagesDAL'
import createError from 'http-errors'

export default class AssetsController {
    static async getAssets() {
        console.log("\tAssetsController@getAssets")
        const res = await assetsDal.get()
        return res ? res: false
    }


    static async getOne(id) {
        console.log("\tAssetsController@getAssets")
        const res = await assetsDal.findOne({asset_id: id})
        return res ? res: false
    }

    static async linkAsset(body, id) {
        console.log("\tAssetsController@linkAsset");
        const card = await cardsDal.findOne({id: id})
        if(!card) throw createError(400, 'Card does not exists')
        const image = await imagesDal.findOne({id: body.image_id})
        if(!image) throw createError(400, 'Image does not exists')
        let asset = await assetsDal.findOne({asset_id: body.asset_id})
        if(asset) throw createError(400, 'Asset already exists')
        asset = await assetsDal.create(body)
        await asset.setImage(image)
        const res = await card.addAsset(asset)

        return res ? res: false
    }

    static async editAsset(body, id) {
        const asset = await assetDal.findOne({asset_id: id})
        if(!asset) throw createError(400, 'Asset already exists')
        const res = await assetsDal.update({
            asset_id: body.asset_id,
            image_id: body.image_id,
            rarity: body.rarity,
            price: body.price
        }, {asset_id: id})
        return res ? res: false
    }
}
