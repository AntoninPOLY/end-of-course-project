
// @ts-nocheck

import { Router } from '@awaitjs/express'
import {authenticateToken} from '../middlewares/auth'
import {checkAsset} from '../middlewares/assets'
import assetsController from './assetsController'
import transacController from '../transactions/transacController'

const router = Router()

router.getAsync("/", async function(_, res) {
    let result = await assetsController.getAssets();
    res.send(result)
});

router.getAsync("/get/:id", async function(req, res) {
    let result = await assetsController.getOne(req.params.id);
    res.send(result)
});

router.getAsync("/amount", async function(_, res) {
   let result = await transacController.amountLeft()
   res.send(result)
})


router.getAsync("/amount/:id", async function(req, res) {
   let result = await transacController.assetLeft(req.params.id)
   res.send(result)
})

router.postAsync("/link/:id", [authenticateToken, checkAsset], async function(req, res) {
    const id = req.params.id
    let result = await assetsController.linkAsset(req.body, id)
    res.send(result)
})

router.postAsync("/edit/:id", [authenticateToken, checkAsset], async function(req, res) {
    const id = req.params.id
    const result = await assetsController.editAsset(req.body, id)
    res.send(result)
})

export default router
