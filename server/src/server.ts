
// @ts-nocheck

import express, { Application, Request, Response } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import path from 'path'

//import users from './users/usersAPI'
import auth from './auth/authAPI'
import cards from './cards/cardsAPI'
import images from './images/imagesAPI'
import assets from './assets/assetsAPI'
import transactions from './transactions/transacAPI'
import dbInit from './db/init'
import algoInit from './algo/init'

dbInit()
algoInit()
//import limitTemplate from 'algosdk/src/logicTemplates/limitorder'

const app: Application = express()

app.use(bodyParser.json());
app.use(cors());

//app.use('/users', users)
app.use('/cards', cards)
app.use('/transactions', transactions)
app.use('/images', images)
app.use('/assets', assets)
app.use('/', auth)

app.use(function(error, req, res, next) {
    console.log(error)
    const code = error.status || 500
    res.status(code)
    res.json({
        status: error.status,
        message: code === 500 ? 'There was an error try again later' : error.message,
    })
})

const port = process.env.PORT || 5000;

const server = app.listen(port)
