// @ts-nocheck

import { Router } from '@awaitjs/express'
import AuthController from './authController'
import {authenticateToken, checkSignup} from '../middlewares/auth'

const router = Router()

router.getAsync('/logout', authenticateToken, async function(_, res ) {
    const result = await AuthController.logout(res.locals.user)
    res.status(200).send(result)
})

router.getAsync('/verify', authenticateToken, async function(_, res ) {
    let result = await AuthController.verify(res.locals.user)
    res.status(200).send(result)
})

router.postAsync('/login', checkSignup, async function(req, res) {
    const result = await AuthController.login(req.body);
    res.status(200).send(result);
})

export default router
