// @ts-nocheck

import * as usersDAL from '../users/usersDAL'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { checkSignup } from '../middlewares'
import createError from 'http-errors'

export default class AuthController {
    static async login(params) {
        console.log("\tAuthController@login");
        let user = await usersDAL.findOne({username: params.username})
        if(user) {
            const compare = await bcrypt.compare(params.password, user.password())
            if(!compare) {
                throw createError(400, 'Passwords do not match')
            }
            await usersDAL.update({ token: generateAccessToken(user.username)}, {username: user.username})
            user = await usersDAL.findOne({username: params.username})
            console.log(user.username + 'is logged in' + Date.now())
            return {
                id: user.id,
                username: user.username,
                token: user.token()
            }
        }

        throw createError(404, 'User not found')
    }

    static async verify(username) {
        console.log("\rAuthController@verify")
        const user = await usersDAL.findOne({username: username.data})
        if(user) {
            return {
                data: {
                    username: user.username,
                }
            }
        }
        return false
    }

    static async logout(username) {
        console.log("\rAuthController@logout")
        const user = await usersDAL.findOne({username: username.data})
        if(user) {
            await usersDAL.update({ token: ''}, {username: user.username})
            return true
        }
        return false
    }
}

function generateAccessToken(username) {
    console.log("\rAuthController@generateToken");
    return jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60 * 60),
        data: username
    }, process.env.TOKEN_SECRET)
}
