
// @ts-nocheck

import { RequestHandler as Middleware } from 'express'
import jwt from 'jsonwebtoken'
import validator from 'validator'
import { IncomingHttpHeaders } from 'http';
import createError from 'http-errors'

const getTokenFromHeaders = (headers: IncomingHttpHeaders) => {
    const header = headers.authorization as string

    if (!header)
    return header

    return header.split(' ')[1]
}

export const authenticateToken: Middleware  = (req, res, next) => {
    console.log("\rAuthMiddleware@authenticateToken")
    const token = getTokenFromHeaders(req.headers) || ''

    if(!token) throw createError(403, 'Invalid token')
    jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
        if(err) throw createError(403, 'Invalid token')
        res.locals.user = decoded
        next()
    })
}

export const checkSignup: Middleware = (req, res, next) => {
    if(validator.isEmpty(req.body.password + '') || validator.isEmpty(req.body.username + '')) {
        throw createError(400, 'Username or password is missing')
    }
    next()
}
